﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class Instructor
    {
        [Key]
        [Required(ErrorMessage = "El campo {0} es oblicatorio")]
        [Display(Name = "Primer Nombre")]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "el nombre no tiene la cantidad necesaria de caracteres (minimo:2- maximo:25)")]
        public string name_instructor { get; set; }

        [Display(Name = "segundo Nombre")]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "el nombre no tiene la cantidad necesaria de caracteres (minimo:2- maximo:25)")]
        public string second_name { get; set; }

        [Required(ErrorMessage = "El campo {0} es oblicatorio")]
        [Display(Name = "Primer Apellido")]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "el nombre no tiene la cantidad necesaria de caracteres (minimo:2- maximo:25)")]
        public string last_name { get; set; }

        [Display(Name = "segundo Apellido")]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "el nombre no tiene la cantidad necesaria de caracteres (minimo:2- maximo:25)")]
        public string second_last_name { get; set; }

        [Required(ErrorMessage = "Email es obligatorio")]
        [RegularExpression("^[_a-z0-9-]+(.[_a-z0-9-]+)@(outlook.com|gmail.com|zoho.com)$", ErrorMessage = "Correo invalido")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Correo Electronico")]
        public string Email { get; set; }


        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        public int id_name_speciality { get; set; }
        public int id_name_headquarter { get; set; }

        public virtual Speciality Specialities { get; set; }
        public virtual Headquarter Headquarters { get; set; }

    }
}