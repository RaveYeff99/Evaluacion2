﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class Speciality
    {
        [Key]
        public int id_name_speciality { get; set; }

        [Required(ErrorMessage = "El campo {0} es oblicatorio")]
        [Display(Name = "Nombre de la especialidad")]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "el nombre no tiene la cantidad necesaria de caracteres (minimo:2- maximo:25)")]
        public string name_speciality { get; set; }

        public virtual ICollection<Instructor> Instructors { get; set; }
    }
}