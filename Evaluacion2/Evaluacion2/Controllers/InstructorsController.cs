﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Evaluacion2.Models;

namespace Evaluacion2.Controllers
{
    public class InstructorsController : Controller
    {
        private Evaluacion2Context db = new Evaluacion2Context();

        // GET: Instructors
        public ActionResult Index()
        {
            var instructors = db.Instructors.Include(i => i.Headquarters).Include(i => i.Specialities);
            return View(instructors.ToList());
        }

        // GET: Instructors/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instructor instructor = db.Instructors.Find(id);
            if (instructor == null)
            {
                return HttpNotFound();
            }
            return View(instructor);
        }

        // GET: Instructors/Create
        public ActionResult Create()
        {
            ViewBag.id_name_headquarter = new SelectList(db.Headquarters, "id_name_headquarter", "name_headquarter");
            ViewBag.id_name_speciality = new SelectList(db.Specialities, "id_name_speciality", "name_speciality");
            return View();
        }

        // POST: Instructors/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "name_instructor,second_name,last_name,second_last_name,Email,Fecha,id_name_speciality,id_name_headquarter")] Instructor instructor)
        {
            if (ModelState.IsValid)
            {
                db.Instructors.Add(instructor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_name_headquarter = new SelectList(db.Headquarters, "id_name_headquarter", "name_headquarter", instructor.id_name_headquarter);
            ViewBag.id_name_speciality = new SelectList(db.Specialities, "id_name_speciality", "name_speciality", instructor.id_name_speciality);
            return View(instructor);
        }

        // GET: Instructors/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instructor instructor = db.Instructors.Find(id);
            if (instructor == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_name_headquarter = new SelectList(db.Headquarters, "id_name_headquarter", "name_headquarter", instructor.id_name_headquarter);
            ViewBag.id_name_speciality = new SelectList(db.Specialities, "id_name_speciality", "name_speciality", instructor.id_name_speciality);
            return View(instructor);
        }

        // POST: Instructors/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "name_instructor,second_name,last_name,second_last_name,Email,Fecha,id_name_speciality,id_name_headquarter")] Instructor instructor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(instructor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_name_headquarter = new SelectList(db.Headquarters, "id_name_headquarter", "name_headquarter", instructor.id_name_headquarter);
            ViewBag.id_name_speciality = new SelectList(db.Specialities, "id_name_speciality", "name_speciality", instructor.id_name_speciality);
            return View(instructor);
        }

        // GET: Instructors/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instructor instructor = db.Instructors.Find(id);
            if (instructor == null)
            {
                return HttpNotFound();
            }
            return View(instructor);
        }

        // POST: Instructors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Instructor instructor = db.Instructors.Find(id);
            db.Instructors.Remove(instructor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
